llamadoFetch = () => {
    const url = "https://restcountries.com/v3.1/name/" + document.getElementById('pais').value;
    fetch(url)
        .then(respuesta => respuesta.json())
            .then(data => mostrarPais(data))
                .catch((reject) => {
                    console.log("Algo ha salido mal" + reject);
                });
}

mostrarPais = (data) => {
    console.log(data);
    document.getElementById('capital').innerHTML =  data[0].capital;
    document.getElementById('lenguaje').innerHTML = Object.values(data[0].languages);
}

//Programar el boton

document.getElementById('btnBuscar').addEventListener('click', function () {
    if (document.getElementById('pais').value == ''){
        alert('Debes escribir un país');
        } else {
            llamadoFetch(); 
        };
});

//Boton limpiar
document.getElementById('btnLimpiar').addEventListener('click', function () {
    document.getElementById('capital').innerHTML = " "
    document.getElementById('lenguaje').innerHTML = " "
});
