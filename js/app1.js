// Realizar la petición HTTP usando Axios
const fetchUsers = () => {
    console.log("Entrando");
    const userId = document.getElementById('id').value;
    const url = `https://jsonplaceholder.typicode.com/users/${userId}`;

    axios.get(url)
        .then(response => response.data)
        .then(data => mostrarResultado(data))
        .catch(error => {
            console.error("Error al realizar la solicitud", error);
        });
}

// Función para mostrar el JSON obtenido
const mostrarResultado = (data) => {
    document.getElementById('nombre').value = data.name;
    document.getElementById('username').value = data.username;
    document.getElementById('email').value = data.email;
    document.getElementById('street').value = data.address.street;
    document.getElementById('suite').value = data.address.suite;
    document.getElementById('city').value = data.address.city;
}

// Programar el botón para hacer la búsqueda
document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('btnBuscar').addEventListener('click', function () {
        fetchUsers();
    });
});
